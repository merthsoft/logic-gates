#ifndef GATE_UPDATE_H
#define GATE_UPDATE_H

#include "gate.h"

void gate_update_and(Gate* gate);
void gate_update_or(Gate* gate);
void gate_update_xor(Gate* gate);

void gate_update_not(Gate* gate);

void gate_update_nand(Gate* gate);
void gate_update_nor(Gate* gate);
void gate_update_xnor(Gate* gate);

void gate_update_empty(Gate* gate);
void gate_update_input(Gate* gate);

void gate_update_circuit(Gate* gate);

#endif
