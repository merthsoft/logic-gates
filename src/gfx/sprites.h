// Converted using ConvPNG
// This file contains all the graphics for easier inclusion in a project
#ifndef sprites_H
#define sprites_H
#include <stdint.h>

extern uint8_t sprites_transpcolor_index;
extern uint8_t palette[5];
#define palette_width 5
#define palette_height 1
extern uint8_t sprite_and_gate[225];
#define sprite_and_gate_width 15
#define sprite_and_gate_height 15
extern uint8_t sprite_or_gate[225];
#define sprite_or_gate_width 15
#define sprite_or_gate_height 15
extern uint8_t sprite_not_gate[225];
#define sprite_not_gate_width 15
#define sprite_not_gate_height 15
extern uint8_t sprite_xor_gate[225];
#define sprite_xor_gate_width 15
#define sprite_xor_gate_height 15
extern uint8_t sprite_input_gate[225];
#define sprite_input_gate_width 15
#define sprite_input_gate_height 15
extern uint16_t sprites_pal[5];

#endif
