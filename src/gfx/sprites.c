// Converted using ConvPNG
// This file contains all the graphics for easier inclusion in a project
#include <stdint.h>
#include "sprites.h"

uint8_t sprites_transpcolor_index = 4;

uint16_t sprites_pal[5] = {
 0xFFFF,  // 00 :: rgba(255,255,255,255)
 0x0000,  // 01 :: rgba(0,0,0,255)
 0x0285,  // 02 :: rgba(0,161,41,255)
 0xF060,  // 03 :: rgba(230,28,0,255)
 0x7C1B,  // 04 :: rgba(255,0,220,255)
};