/* Keep these headers */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <tice.h>
#include <debug.h>

/* Standard headers - it's recommended to leave them included */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gate.h"
#include "gate_update.h"

void gate_update_and(Gate* gate) {
    uint8_t i;
    gate->State = GateState_High;
    for (i = 0; i < gate->NumInputs; i++) {
        gate->State = (GateState)(gate->State && gate->Inputs[i]->State);
        if (gate->State == GateState_Low) { return; }
    }
}

void gate_update_or(Gate* gate) {
    uint8_t i;
    gate->State = GateState_Low;
    for (i = 0; i < gate->NumInputs; i++) {
        gate->State = (GateState)(gate->State || gate->Inputs[i]->State);
        if (gate->State == GateState_High) { return; }
    }
}

void gate_update_xor(Gate* gate) {
    uint8_t i;
    gate->State = gate->Inputs[0]->State;
    for (i = 1; i < gate->NumInputs; i++) {
        gate->State = (GateState)(gate->State ^ gate->Inputs[i]->State);
    }
}

void gate_update_not(Gate* gate) {
    gate->State = (GateState)(!gate->Inputs[0]->State);
}

void gate_update_nand(Gate* gate) {
    gate_update_and(gate);
    gate->State = !gate->State;
}

void gate_update_nor(Gate* gate) {
    gate_update_or(gate);
    gate->State = !gate->State;
}

void gate_update_xnor(Gate* gate) {
    gate_update_xor(gate);
    gate->State = !gate->State;
}

void gate_update_empty(Gate* gate) {}

void gate_update_input(Gate* gate) {}

void gate_update_circuit(Gate* gate) {
    List* trav;
    Gate* g;
    Circuit* circ = (Circuit*)gate;
    if (circ->Gates == NULL) { return; }

    trav = circ->Gates;
    while (trav->Next != NULL) {
        trav = trav->Next;
        g = (Gate*)trav->Data;

        if (g->Update == gate_update_input) {
            InputGate* iGate = (InputGate*)g;
            iGate->Base.State = circ->Base.Inputs[iGate->InputNumber]->State;
        }
    }

    gate_process(circ->Gates);

    circ->Base.State = circ->OutputGate->State;
}