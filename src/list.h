#ifndef LIST_H
#define LIST_H

typedef struct List {
    struct List* Prev;
    struct List* Next;
    void* Data;
} List;

List* list_add(List* list, void* data);
void list_add_range(List* list1, List* list2);
void list_delete(List* list);
void list_remove(List* list);
List* list_create();

#endif
