/* Keep these headers */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <tice.h>
#include <debug.h>

/* Standard headers - it's recommended to leave them included */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "gate.h"
#include "list.h"
#include "gate_update.h"
#include "gfx/sprites.h"

Gate* emptyGate;

void gate_init() {
    emptyGate = gate_create(0, gate_update_empty, NULL, NULL);
}

void gate_shutdown() {
    gate_delete(emptyGate);
}

Gate* gate_create(uint8_t numInputs, void(*function)(Gate*), char* label, uint8_t* sprite) {
    uint8_t i;
    Gate* gate = (Gate*)malloc(sizeof(Gate));

    gate->NumInputs = numInputs;

    if (numInputs == 0) {
        gate->Inputs = NULL;
    } else {
        gate->Inputs = (Gate**)malloc(numInputs * sizeof(Gate*));

        for (i = 0; i < numInputs; i++) {
            gate->Inputs[i] = emptyGate;
        }
    }

    gate->Update = function;
    gate->Outputs = list_create();
    gate->State = GateState_Low;
    gate->Generation = 0;
    gate->Label = label;
    gate->Sprite = sprite;
    gate->XLocation = 0;
    gate->YLocation = 0;

    return gate;
}

void gate_link(Gate* gate1, Gate* gate2, uint8_t inputNum) {
    list_add(gate1->Outputs, gate2);
    gate2->Inputs[inputNum] = gate1;
}

void gate_process(List* gates) {
    Gate* gate;
    List* queue;
    List* trav = gates;
    uint8_t generation = 0;
    sprintf(dbgout, "Entering gate_process: %p.\n", gates);

    queue = list_create();

    sprintf(dbgout, "Creating queue.\n");
    while (trav->Next != NULL) {
        trav = trav->Next;
        gate = (Gate*)trav->Data;
        gate->Generation = 0;
        if (gate->Update == gate_update_input) {
            list_add(queue, gate);
            generation++;
        }
    }
    sprintf(dbgout, "Queue created.\n");

    sprintf(dbgout, "Processing queue.\n");
    while (queue->Next != NULL) {
        sprintf(dbgout, "Processing item: ");
        queue = queue->Next;
        gate = (Gate*)queue->Data;
        sprintf(dbgout, "%s.\n", gate->Label);
        gate->Generation++;

        if (gate->Generation <= generation) {
            void(*update)(Gate*) = gate->Update;
            update(gate);
            sprintf(dbgout, "Adding connected gates to queue.\n");
            list_add_range(queue, gate->Outputs);
        }
        sprintf(dbgout, "Item processed.\n");
    }
    sprintf(dbgout, "Queue processed.\n");
    list_delete(queue);
}

void gate_delete(Gate* gate) {
    free(gate);
}

Gate* gate_create_and(uint8_t numInputs) {
    return gate_create(numInputs, gate_update_and, "&", sprite_and_gate);
}

Gate* gate_create_or(uint8_t numInputs) {
    return gate_create(numInputs, gate_update_or, "|", sprite_or_gate);
}

Gate* gate_create_xor(uint8_t numInputs) {
    return gate_create(numInputs, gate_update_xor, "^", sprite_xor_gate);
}

Gate* gate_create_not() {
    return gate_create(1, gate_update_not, "!", sprite_not_gate);
}

Gate* gate_create_input(uint8_t inputNumber) {
    char* inputLabel;
    Gate* gate;
    InputGate* iGate;
    
    inputLabel = malloc(8);
    sprintf(inputLabel, "I%d", inputNumber);
    gate = gate_create(0, gate_update_input, inputLabel, sprite_input_gate);
    iGate = malloc(sizeof(InputGate));
    iGate->Base = *gate;
    free(gate);
    iGate->InputNumber = inputNumber;

    return (Gate*)iGate;
}

Gate* gate_create_circuit(uint8_t numInputs, char* label, List* gates, Gate* outputGate) {
    Gate* gate; 
    Circuit* circ;
    
    circ = malloc(sizeof(Circuit));
    gate = gate_create(numInputs, gate_update_circuit, label, NULL);
    
    circ->Base = *gate;
    circ->Gates = list_create();
    circ->OutputGate = outputGate;

    free(gate);

    if (gates == NULL) {
        return (Gate*)circ;
    }

    gates = gates->Next;
    while (gates != NULL) {
        list_add(circ->Gates, gates->Data);
        gates = gates->Next;
    }

    return (Gate*)circ;
}

Gate* gate_create_nand(uint8_t numInputs) {
    return gate_create(numInputs, gate_update_nand, "!&", NULL);
}

Gate* gate_create_nor(uint8_t numInputs) {
    return gate_create(numInputs, gate_update_nor, "!|", NULL);
}

Gate* gate_create_xnor(uint8_t numInputs) {
    return gate_create(numInputs, gate_update_xnor, "!^", NULL);
}