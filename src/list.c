/* Keep these headers */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <tice.h>
#include <debug.h>

/* Standard headers - it's recommended to leave them included */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

List* list_add(List* list, void* data) {
    while (list->Next != NULL) {
        list = list->Next;
    }

    list->Next = (List*)malloc(sizeof(List));
    list->Next->Prev = list;
    list->Next->Data = data;
    list->Next->Next = NULL;

    return list->Next;
}

void list_add_range(List* list1, List* list2) {
    list2 = list2->Next;
    while (list2 != NULL) {
        list1 = list_add(list1, list2->Data);
        list2 = list2->Next;
    }
}

void list_delete(List* list) {
    while (list->Next != NULL) {
        list = list->Next;
    }
    while (list->Prev != NULL) {
        list = list->Prev;
        free(list->Next);
    }
    free(list);
}

void list_remove(List* list) {
    if (list->Prev != NULL) { list->Prev->Next = list->Next; }
    if (list->Next != NULL) { list->Next->Prev = list->Prev; }
}

List* list_create() {
    List* list = (List*)malloc(sizeof(List));
    list->Data = NULL;
    list->Next = NULL;
    list->Prev = NULL;

    return list;
}
