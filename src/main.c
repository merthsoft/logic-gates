/* Keep these headers */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <tice.h>
#include <debug.h>

/* Standard headers - it's recommended to leave them included */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Shared libraries */
#include <lib\ce\graphc.h>

#include "gate.h"
#include "list.h"
#include "key_helper.h"
#include "gfx\sprites.h"

#define LCD_WIDTH_PX 320
#define LCD_HEIGHT_PX 240

#define SPRITE_WIDTH sprite_and_gate_width
#define SPRITE_HEIGHT sprite_and_gate_height

#define GRID_WIDTH (SPRITE_WIDTH / 4)
#define GRID_HEIGHT (SPRITE_HEIGHT / 4)

#define BACKGROUND_COLOR 0
#define FOREGROUND_COLOR 1

#define LOW_COLOR 3
#define HIGH_COLOR 2

void draw_object(Gate* object) {
    List* output;

    uint8_t color = object->State == GateState_Low ? LOW_COLOR : HIGH_COLOR;
    gc_SetColorIndex(color);
    gc_NoClipRectangle(GRID_WIDTH * object->XLocation, GRID_HEIGHT * object->YLocation, SPRITE_WIDTH, SPRITE_HEIGHT);
    gc_NoClipDrawTransparentSprite(object->Sprite, GRID_WIDTH * object->XLocation, GRID_HEIGHT * object->YLocation, SPRITE_WIDTH, SPRITE_HEIGHT);
    
    if (object->Outputs != NULL) {
        output = object->Outputs->Next;
        while (output != NULL) {
            Gate* out;
            out = (Gate*)output->Data;
            gc_NoClipLine(GRID_WIDTH * object->XLocation + SPRITE_WIDTH, GRID_HEIGHT * object->YLocation + GRID_HEIGHT * 2, 
                          GRID_WIDTH * out->XLocation, GRID_HEIGHT * out->YLocation + GRID_HEIGHT * 2);
            output = output->Next;
        }
    }
}

List* create_xor() {
    List* gates = list_create();
    Gate* in1 = gate_create_input(0);
    Gate* in2 = gate_create_input(1);

    Gate* not1 = gate_create_not();
    Gate* not2 = gate_create_not();

    Gate* and1 = gate_create_and(2);
    Gate* and2 = gate_create_and(2);

    Gate* or = gate_create_or(2);

    in1->XLocation = 5;
    in1->YLocation = 5;

    in2->XLocation = 5;
    in2->YLocation = 15;

    not1->XLocation = 20;
    not1->YLocation = 5;

    not2->XLocation = 20;
    not2->YLocation = 15;

    and1->XLocation = 30;
    and1->YLocation = 5;

    and2->XLocation = 30;
    and2->YLocation = 15;

    or->XLocation = 45;
    or->YLocation = 10;

    gate_link(in1, not1, 0);
    gate_link(in1, and2, 0);

    gate_link(in2, not2, 0);
    gate_link(in2, and1, 0);

    gate_link(not1, and1, 1);
    gate_link(not2, and2, 1);

    gate_link(and1, or , 0);
    gate_link(and2, or , 1);

    list_add(gates, in1);
    list_add(gates, in2);
    list_add(gates, not1);
    list_add(gates, not2);
    list_add(gates, and1);
    list_add(gates, and2);
    list_add(gates, or);

    return gates;
}

void main(void) {
    uint8_t i = 0;
    List* xor;
    List* trav;
    Gate* in1;
    Gate* in2;

    key_init();
    gc_InitGraph();

    gc_SetPalette(sprites_pal, sizeof(sprites_pal));
    gc_SetTransparentColor(sprites_transpcolor_index);
    gc_FillScrn(BACKGROUND_COLOR);

    xor = create_xor();
    in1 = xor->Next->Data;
    in2 = xor->Next->Next->Data;

    do {
        trav = xor->Next;
        while (trav != NULL) {
            draw_object((Gate*)trav->Data);
            trav = trav->Next;
        }

        if (in1->State == GateState_Low && in2->State == GateState_Low) {
            in1->State = GateState_High;
        } else if (in1->State == GateState_High && in2->State == GateState_Low) {
            in1->State = GateState_Low;
            in2->State = GateState_High;
        } else if (in1->State == GateState_Low && in2->State == GateState_High) {
            in1->State = GateState_High;
            in2->State = GateState_High;
        } else if (in1->State == GateState_High && in2->State == GateState_High) {
            in1->State = GateState_Low;
            in2->State = GateState_Low;
        }

        sprintf(dbgout, "Before gate_process: %p.\n", xor);
        gate_process(xor);

        while (!key_is_down(Key_2nd) && !key_is_down(Key_Del)) {
            key_scan_keys(0);
        }
    } while (!key_is_down(Key_Del));

    key_reset();
    gc_CloseGraph();
    pgrm_CleanUp();
}

