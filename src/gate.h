#ifndef GATE_H
#define GATE_H

#include <stdint.h>

#include "list.h"

typedef void* GateFunction;

typedef enum GateState {
    GateState_Low = 0,
    GateState_High = 1,
} GateState;

typedef struct Gate {
    GateState State;
    struct Gate** Inputs;
    List* Outputs;
    uint8_t NumInputs;
    GateFunction Update;
    uint8_t Generation;
    char* Label;
    uint16_t XLocation;
    uint16_t YLocation;
    uint8_t* Sprite;
} Gate;

typedef struct Circuit {
    Gate Base;
    List* Gates;
    Gate* OutputGate;
} Circuit;

typedef struct InputGate {
    Gate Base;
    uint8_t InputNumber;
} InputGate;

void gate_init();
void gate_shutdown();

void gate_link(Gate* gate1, Gate* gate2, uint8_t inputNum);
void gate_process(List* gates);

Gate* gate_create(uint8_t numInputs, void(*function)(Gate*), char* label, uint8_t* sprite);

Gate* gate_create_and(uint8_t numInputs);
Gate* gate_create_or(uint8_t numInputs);
Gate* gate_create_xor(uint8_t numInputs);

Gate* gate_create_not();

Gate* gate_create_nand(uint8_t numInputs);
Gate* gate_create_nor(uint8_t numInputs);
Gate* gate_create_xnor(uint8_t numInputs);

Gate* gate_create_input(uint8_t inputNumber);

Gate* gate_create_circuit(uint8_t numInputs, char* label, List* gates, Gate* outputGate);

void gate_delete(Gate* gate);

#endif 
